<!DOCTYPE html>
<html>
	@include('template.head')
<body class="loading-overlay-showing" data-plugin-page-transition data-loading-overlay data-plugin-options="{'hideDelay': 500}">
	<div class="loading-overlay">
		<div class="bounce-loader">
			<div class="bounce1"></div>
			<div class="bounce2"></div>
			<div class="bounce3"></div>
		</div>
	</div>

	<div class="body">
	@include('informasi.header')
	@include('informasi.isikonten')
	@include('template.content')
		
	@include('template.footer')
		
	</div>

	<!-- Vendor -->
	@include('template.setting')


</body>
</html>
