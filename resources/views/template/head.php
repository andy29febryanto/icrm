<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>Pusdatin JAMSOS DKI JAKARTA</title>	

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="iCRM">
		<meta name="TA" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="../assets/img/logo_pusdatin.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="../assets/img/logo pusdatin1.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../assets/vendor/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="../assets/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="../assets/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="../assets/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="../assets/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="../assets/vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="../assets/css/theme.css">
		<link rel="stylesheet" href="../assets/css/theme-elements.css">
		<link rel="stylesheet" href="../assets/css/theme-blog.css">
		<link rel="stylesheet" href="../assets/css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="../assets/vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="../assets/vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="../assets/vendor/rs-plugin/css/navigation.css">
		
		<!-- Demo CSS -->


		<!-- Skin CSS -->
		<link rel="stylesheet" href="../assets/css/skins/skin-corporate-9.css"> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="../assets/css/custom.css">

		<!-- Head Libs -->
		<script src="../assets/vendor/modernizr/modernizr.min.js"></script>

	</head>
	<div class="body">
		