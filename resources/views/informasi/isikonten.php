	<section class="section section-default border-0">
			<div class="container">

					<div class="row">
						<div class="col">
							<h4 class="mb-2 mt-5">Informasi Pengaduan</h4>

							<div class="process process-vertical py-4">
								<div class="process-step appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">
									<div class="process-step-circle">
										<strong class="process-step-circle-content">1</strong>
									</div>
									<div class="process-step-content">
										<h4 class="mb-1 text-4 font-weight-bold">First Step</h4>
										<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas risus est, dignissim non urna at, efficitur cursus nibh. Curabitur varius vel mauris at auctor. Nam ullamcorper bibendum est et porta. Donec congue libero vitae semper varius. Vivamus eu congue leo. Morbi molestie aliquet magna ac dignissim. Quisque ullamcorper elit at metus elementum condimentum. Sed orci augue, pellentesque in egestas quis, vestibulum non ex. Suspendisse tempus tellus ac augue iaculis suscipit.</p>
									</div>
								</div>
								<div class="process-step appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">
									<div class="process-step-circle">
										<strong class="process-step-circle-content">2</strong>
									</div>
									<div class="process-step-content">
										<h4 class="mb-1 text-4 font-weight-bold">Second Step</h4>
										<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas risus est, dignissim non urna at, efficitur cursus nibh. Curabitur varius vel mauris at auctor. Nam ullamcorper bibendum est et porta. Donec congue libero vitae semper varius. Vivamus eu congue leo. Morbi molestie aliquet magna ac dignissim.</p>
									</div>
								</div>
								<div class="process-step appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="600">
									<div class="process-step-circle">
										<strong class="process-step-circle-content">3</strong>
									</div>
									<div class="process-step-content">
										<h4 class="mb-1 text-4 font-weight-bold">Third Step</h4>
										<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas risus est, dignissim non urna at, efficitur cursus nibh. Curabitur varius vel mauris at auctor. Nam ullamcorper bibendum est et porta. Donec congue libero vitae semper varius. Vivamus eu congue leo. Quisque ullamcorper elit at metus elementum condimentum. Sed orci augue, pellentesque in egestas quis, vestibulum non ex. Suspendisse tempus tellus ac augue iaculis suscipit.</p>
									</div>
								</div>
								<div class="process-step appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800">
									<div class="process-step-circle">
										<strong class="process-step-circle-content">4</strong>
									</div>
									<div class="process-step-content">
										<h4 class="mb-1 text-4 font-weight-bold">Fourth Step</h4>
										<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas risus est, dignissim non urna at, efficitur cursus nibh. Curabitur varius vel mauris at auctor.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>
	</section>
	<section class="parallax section section-height-2 section-parallax m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="../assets/img/parallax/parallax-corporate-14-1.jpg">
				<div class="container">
					