	
				<!-- If data available -->
				<!-- <div class="row featured-boxes featured-boxes-style-4">
					<div class="col-md-6 col-lg-3">
						<div class="featured-box featured-box-primary featured-box-effect-4 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200">
							<div class="box-content px-4">
								<i class="icon-featured icon-screen-tablet icons text-12"></i>
								<h4 class="font-weight-bold text-color-dark pb-1 mb-2">Mobile Apps</h4>
								<p class="mb-0">Lorem ipsum dolor sit amet, coctetur adipiscing elit.</p>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3">
						<div class="featured-box featured-box-primary featured-box-effect-4 appear-animation" data-appear-animation="fadeIn">
							<div class="box-content px-4">
								<i class="icon-featured icon-layers icons text-12"></i>
								<h4 class="font-weight-bold text-color-dark pb-1 mb-2">Creative Websites</h4>
								<p class="mb-0">Lorem ipsum dolor sit amet, coctetur adipiscing elit.</p>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3">
						<div class="featured-box featured-box-primary featured-box-effect-4 appear-animation" data-appear-animation="fadeIn">
							<div class="box-content px-4">
								<i class="icon-featured icon-magnifier icons text-12"></i>
								<h4 class="font-weight-bold text-color-dark pb-1 mb-2">SEO Optimization</h4>
								<p class="mb-0">Lorem ipsum dolor sit amet, coctetur adipiscing elit.</p>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3">
						<div class="featured-box featured-box-primary featured-box-effect-4 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">
							<div class="box-content px-4">
								<i class="icon-featured icon-screen-desktop icons text-12"></i>
								<h4 class="font-weight-bold text-color-dark pb-1 mb-2">Brand Solutions</h4>
								<p class="mb-0">Lorem ipsum dolor sit amet, coctetur adipiscing elit.</p>
							</div>
						</div>
					</div>
				</div> -->
				<!-- -->
			


			<section class="parallax section section-height-2 section-parallax m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="../assets/img/parallax/parallax-corporate-14-1.jpg">
				<div class="container">
					<div class="row justify-content-center text-center mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">
									<div class="row counters counters-lg counters-text-dark">
											<div class="counter">
												<strong class="font-weight-extra-bold text-12" data-to="200" data-append="+">0</strong>
												<label class="opacity-8 font-weight-normal text-4">Jumlah Aduan</label>
											</div>
									</div>
					</div>
				</div>
			</section>
			<hr>
		
			<div class="container">
				<div class="row py-4 my-5">
					<div class="col py-3">
						<div class="owl-carousel owl-theme mb-0" data-plugin-options="{'responsive': {'0': {'items': 1}, '476': {'items': 1}, '768': {'items': 5}, '992': {'items': 7}, '1200': {'items': 7}}, 'autoplay': true, 'autoplayTimeout': 3000, 'dots': false}">
							<div>
								<img class="img-fluid opacity-2" src="../assets/img/logos/logo-1.png" alt="">
							</div>
							<div>
								<img class="img-fluid opacity-2" src="../assets/img/logos/logo-2.png" alt="">
							</div>
							<div>
								<img class="img-fluid opacity-2" src="../assets/img/logos/logo-3.png" alt="">
							</div>
							<div>
								<img class="img-fluid opacity-2" src="../assets/img/logos/logo-4.png" alt="">
							</div>
							<div>
								<img class="img-fluid opacity-2" src="../assets/img/logos/logo-5.png" alt="">
							</div>
							<div>
								<img class="img-fluid opacity-2" src="../assets/img/logos/logo-6.png" alt="">
							</div>
							<div>
								<img class="img-fluid opacity-2" src="../assets/img/logos/logo-4.png" alt="">
							</div>
							<div>
								<img class="img-fluid opacity-2" src="../assets/img/logos/logo-2.png" alt="">
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>