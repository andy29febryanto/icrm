<section class="parallax section section-height-2 section-parallax m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="../assets/img/parallax/parallax-corporate-1-1.jpg">
		<div class="container py-4 my-5">
							<div class="accordion accordion-modern" id="accordion">
								<div class="card card-default">
									<div class="card-header">
										<h4 class="card-title m-0">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
												Form Pengaduan
											</a>
										</h4>
									</div>
									<div id="collapseOne" class="collapse show">
										<div class="card-body">
											<form action="/" id="frmBillingAddress" method="post">
												<!-- <div class="form-row">
													<div class="form-group col">
														<label class="font-weight-bold text-dark text-2">Country</label>
														<select class="form-control">
															<option value="">Select a country</option>
														</select>
													</div>
												</div> -->
												<div class="form-row">
													<div class="form-group col-lg-6">
														<label class="font-weight-bold text-dark text-2">Nama Lengkap</label>
														<input type="text" value="" class="form-control">
													</div>
													<div class="form-group col-lg-6">
														<label class="font-weight-bold text-dark text-2">NIK</label>
														<input type="text" value="" class="form-control">
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-lg-6">
														<label class="font-weight-bold text-dark text-2">No. Handphone</label>
														<input type="text" value="" class="form-control">
													</div>
													<div class="form-group col-lg-6">
														<label class="font-weight-bold text-dark text-2">E-Mail</label>
														<input type="text" value="" class="form-control">
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-lg-6">
														<label class="font-weight-bold text-dark text-2">Topik Pengaduan</label>
														<select class="form-control">
															<option value="">Pilih salah satu</option>
														</select>
													</div>
													<div class="form-group col-lg-6">
														<label class="font-weight-bold text-dark text-2">Jenis Pertanyaan</label>
														<select class="form-control">
															<option value="">Pilih salah satu</option>
														</select>
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col">
														<label class="font-weight-bold text-dark text-2">Pengaduan</label>
															<textarea maxlength="5000" data-msg-required="Silahkan Ketik Disini." rows="8" class="form-control" name="message" id="message" required></textarea>

													</div>
												</div>
												<div class="form-row">
													<div class="form-group col">
														<input type="submit" value="Submit" class="btn btn-xl btn-light pr-4 pl-4 text-2 font-weight-semibold text-uppercase float-right mb-2" data-loading-text="Loading...">
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
</section>

