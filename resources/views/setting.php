<script src="../public/assets/vendor/jquery/jquery.min.js"></script>
	<script src="../public/assets/vendor/jquery.appear/jquery.appear.min.js"></script>
	<script src="../public/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
	<script src="../public/assets/vendor/jquery.cookie/jquery.cookie.min.js"></script>
	<script src="../public/assets/vendor/popper/umd/popper.min.js"></script>
	<script src="../public/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="../public/assets/vendor/common/common.min.js"></script>
	<script src="../public/assets/vendor/jquery.validation/jquery.validate.min.js"></script>
	<script src="../public/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="../public/assets/vendor/jquery.gmap/jquery.gmap.min.js"></script>
	<script src="../public/assets/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
	<script src="../public/assets/vendor/isotope/jquery.isotope.min.js"></script>
	<script src="../public/assets/vendor/owl.carousel/owl.carousel.min.js"></script>
	<script src="../public/assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="../public/assets/vendor/vide/jquery.vide.min.js"></script>
	<script src="../public/assets/vendor/vivus/vivus.min.js"></script>
	
	<!-- Theme Base, Components and Settings -->
	<script src="../public/assets/js/theme.js"></script>
	
	<!-- Current Page Vendor and Views -->
	<script src="../public/assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="../public/assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	
	<!-- Theme Custom -->
	<script src="../public/assets/js/custom.js"></script>
	
	<!-- Theme Initialization Files -->
	<script src="../public/assets/js/theme.init.js"></script>
	<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
		ga('create', 'UA-12345678-1', 'auto');
		ga('send', 'pageview');
	</script>
	 -->